package com.zs.pig.mall.startup;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.google.common.util.concurrent.AbstractIdleService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@WebListener
public class Bootstrap extends AbstractIdleService  implements ServletContextListener{

    private ClassPathXmlApplicationContext context;
    private static final Logger LOGGER = LoggerFactory.getLogger(Bootstrap.class);

    public static void main(String[] args) {
    	Bootstrap bootstrap = new Bootstrap();
        bootstrap.startAsync();
        try {
            Object lock = new Object();
            synchronized (lock) {
                while (true) {
                    lock.wait();
                }
            }
        } catch (InterruptedException ex) {
        	LOGGER.error("ignore interruption",ex);
        }
    }

    /**
     * Start the service.
     */
    @Override
    protected void startUp() throws Exception {
        LOGGER.info("===================pig-mall START ....==========================");
        context = new ClassPathXmlApplicationContext(new String[]{"config/spring/mall-context.xml"});
        context.start();
        context.registerShutdownHook();
        LOGGER.info("pig-mall service started successfully");
       

    }

    /**
     * Stop the service.
     */
    @Override
    protected void shutDown() throws Exception {
        context.stop();
        LOGGER.info("service stopped successfully");
    }
    /**
     * 
     */
  

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		 try {
	            shutDown();
	     } catch (Exception e) {
	            e.printStackTrace();
	     }
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		 LOGGER.info("pig-mall service started ");
	     try {
	            startUp();
	     } catch (Exception ex) {
	            ex.printStackTrace();
	     LOGGER.error("ignore interruption ");
	     }
	}
}
