package com.zs.pig.mall.api.service;

import java.util.List;

import com.zs.pig.common.base.BaseService;
import com.zs.pig.common.base.model.EUTreeNode;
import com.zs.pig.mall.api.model.TbItemCat;

public interface ItemCatService extends BaseService<TbItemCat>{

	List<EUTreeNode> getCatList(long parentId);
}
