package com.zs.pig.mall.api.service;

import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.zs.pig.common.base.BaseService;

public interface PictureService {

	Map uploadPicture(MultipartFile uploadFile);
}
