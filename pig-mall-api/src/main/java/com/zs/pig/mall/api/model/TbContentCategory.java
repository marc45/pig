package com.zs.pig.mall.api.model;

import java.util.Date;

import javax.persistence.Table;

import com.zs.pig.common.base.BaseEntity;
/**
 * 2016-04-22
 * @author zsCat
 */

@SuppressWarnings({ "unused"})
@Table(name="tb_content_category")
public class TbContentCategory extends BaseEntity {

	private static final long serialVersionUID = 1L;

    private Long parentId;

    private String name;

    private Integer status;

    private Integer sortOrder;

    private Boolean isParent;

    private Date created;

    private Date updated;

  
    public Long getParentId() {
        return this.getLong("parentId");
    }

    public void setParentId(Long parentId) {
        this.set("parentId", parentId);
    }

    public String getName() {
        return this.getString("name");
    }

    public void setName(String name) {
        this.set("name", name);
    }

    public Integer getStatus() {
        return this.getInteger("status");
    }

    public void setStatus(Integer status) {
        this.set("status",status);
    }

    public Integer getSortOrder() {
        return this.getInteger("sortOrder");
    }

    public void setSortOrder(Integer sortOrder) {
        this.set("sortOrder", sortOrder);
    }

    public Boolean getIsParent() {
        return this.getBoolean("isParent");
    }

    public void setIsParent(Boolean isParent) {
        this.set("isParent", isParent);
    }

    public Date getCreated() {
        return this.getDate("created");
    }

    public void setCreated(Date created) {
        this.set("created", created);
    }

    public Date getUpdated() {
        return this.getDate("updated");
    }

    public void setUpdated(Date updated) {
        this.set("updated",updated);
    }
}