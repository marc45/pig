package com.zs.pig.mall.api.model;

import java.util.Date;

import javax.persistence.Table;

import com.zs.pig.common.base.BaseEntity;
/**
 * 2016-04-22
 * @author zsCat
 */

@SuppressWarnings({ "unused"})
@Table(name="tb_order_shipping")
public class TbOrderShipping extends BaseEntity {

	private static final long serialVersionUID = 1L;

    private String receiverName;

    private String receiverPhone;

    private String receiverMobile;

    private String receiverState;

    private String receiverCity;

    private String receiverDistrict;

    private String receiverAddress;

    private String receiverZip;

    private Date created;

    private Date updated;

  

    public String getReceiverName() {
        return this.getString("receiverName");
    }

    public void setReceiverName(String receiverName) {
        this.set("receiverName", receiverName);
    }

    public String getReceiverPhone() {
        return this.getString("receiverPhone");
    }

    public void setReceiverPhone(String receiverPhone) {
        this.set("receiverPhone", receiverPhone);
    }

    public String getReceiverMobile() {
        return this.getString("receiverMobile");
    }

    public void setReceiverMobile(String receiverMobile) {
       this.set("receiverMobile", receiverMobile);
    }

    public String getReceiverState() {
        return this.getString("receiverState");
    }

    public void setReceiverState(String receiverState) {
        this.set("receiverState", receiverState);
    }

    public String getReceiverCity() {
        return this.getString("receiverCity");
    }

    public void setReceiverCity(String receiverCity) {
        this.set("receiverCity", receiverCity);
    }

    public String getReceiverDistrict() {
        return this.getString("receiverDistrict");
    }

    public void setReceiverDistrict(String receiverDistrict) {
        this.set("receiverDistrict", receiverDistrict);
    }

    public String getReceiverAddress() {
        return this.getString("receiverAddress");
    }

    public void setReceiverAddress(String receiverAddress) {
        this.set("receiverAddress", receiverAddress);
    }

    public String getReceiverZip() {
        return this.getString("receiverZip");
    }

    public void setReceiverZip(String receiverZip) {
        this.set("receiverZip", receiverZip);
    }

    public Date getCreated() {
        return this.getDate("created");
    }

    public void setCreated(Date created) {
        this.set("created", created);
    }

    public Date getUpdated() {
        return this.getDate("updated");
    }

    public void setUpdated(Date updated) {
        this.set("updated",updated);
    }
}