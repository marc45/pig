package com.zs.pig.mall.api.model;

import javax.persistence.Table;

import com.zs.pig.common.base.BaseEntity;

/**
 * 2016-04-22
 * @author zsCat
 */

@SuppressWarnings({ "unused"})
@Table(name="tb_order_item")
public class TbOrderItem extends BaseEntity {

	private static final long serialVersionUID = 1L;

    private String itemId;

    private String orderId;

    private Integer num;

    private String title;

    private Long price;

    private Long totalFee;

    private String picPath;

   

    public String getItemId() {
        return this.getString("itemId");
    }

    public void setItemId(String itemId) {
        this.set("itemId", itemId);
    }

    public String getOrderId() {
        return this.getString("orderId");
    }

    public void setOrderId(String orderId) {
        this.set("orderId", orderId);
    }

    public Integer getNum() {
        return this.getInteger("num");
    }

    public void setNum(Integer num) {
        this.set("num", num);
    }

    public String getTitle() {
        return this.getString("title");
    }

    public void setTitle(String title) {
        this.set("title", title);
    }

    public Long getPrice() {
        return this.getLong("price");
    }

    public void setPrice(Long price) {
        this.set("price", price);
    }

    public Long getTotalFee() {
        return this.getLong("totalFee");
    }

    public void setTotalFee(Long totalFee) {
        this.set("totalFee", totalFee);
    }

    public String getPicPath() {
        return this.getString("picPath");
    }

    public void setPicPath(String picPath) {
       this.set("picPath", picPath);
    }
}