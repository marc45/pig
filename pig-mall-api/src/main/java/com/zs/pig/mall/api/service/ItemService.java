package com.zs.pig.mall.api.service;

import com.zs.pig.common.base.BaseService;
import com.zs.pig.common.base.model.EUDataGridResult;
import com.zs.pig.common.base.model.PigResult;
import com.zs.pig.mall.api.model.TbItem;

public interface ItemService extends BaseService<TbItem>{

	TbItem getItemById(long itemId);
	EUDataGridResult getItemList(int page, int rows);
	PigResult createItem(TbItem item, String desc, String itemParam) throws Exception;
}
